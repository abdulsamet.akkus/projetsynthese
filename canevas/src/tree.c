#include <stdlib.h>
#include <stdio.h>
#include "tree.h"

static void preorder(EventNode *node) {
  if (node != NULL) {
    display_point(node->key);
    printf("\n");
    preorder(node->left);
    preorder(node->right);
  }
}

static void inorder(EventNode *node) {
  if (node != NULL) {
    inorder(node->left);
    display_point(node->key);
    printf("\n");
    inorder(node->right);
  }
}

static void postorder(EventNode *node) {
  if (node != NULL) {
    postorder(node->left);
    postorder(node->right);
    display_point(node->key);
    printf("\n");
  }
}

// order = 0 (preorder), 1 (inorder), 2 (postprder)
void display_tree_keys(const EventTree *tree, int order) {
  switch (order) {
    case 0:
      preorder(tree->root);
      break;
    case 1:
      inorder(tree->root);
      break;
    case 2:
      postorder(tree->root);
      break;
    default:
      printf("display_tree_keys: non valid order parameter\n");
      exit(1);
  }
}
//Cette fonction return 1 si les deux point sont identique
//sinon return 0
int eq_key(Point a, Point b)
{
	if(eq(a.x,b.x)==1 && eq(a.y,b.y)==1)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
/*
 * renvoie un nouveau EventNode d'attribut
 *  <key, type, s1, s2, NULL, NULL>
 */
EventNode * new_event(Point key, int type, Segment *s1, Segment *s2)
{
	EventNode * new = (EventNode *)malloc(sizeof(EventNode));
	new->key = key;
	new->type = type;
	new->s1 = s1;
	new->s2 = s2;
	new->left = NULL;
	new->right = NULL;
	return new;
}

// Fonction auxiliaire qui permet une insertion à partir de deux EventNode
void insert_aux(EventNode *node, EventNode *event) 
{
	//Dans le cas ou event n'est pas prioritaire sur node
	if(point_prec(event->key,node->key) == 0) 
	{
		//On l'insere dans le fils gauche
		if(node->left != NULL) 
		{
			insert_aux(node->left, event);
		}
		else 
		{
			node->left = event;
		}
	}
	//Dans le cas ou evet est prioritaire sur node
	else 
	{
		//On l'insere dans le fils droit dans le cas ou
		if(node->right != NULL) 
		{
			insert_aux(node->right, event);
		}
		else 
		{
			node->right = event;
		}
	}
}

/*
 * recherche la place dans tree du nouvel événement event
 * et l'insère.
 * L'ABR est modifié et la modification peut porter sur sa racine.
 */
void insert_event(EventTree *tree, EventNode *event) {
  // A faire
	if(tree->size == 0) {
		tree->root = event;
		tree->size=1;
	}
	else {
		tree->size++;
		insert_aux(tree->root, event);
	}
}
//nous partons du principe que ce parent existe dans cette fonction
//Le noeud en question est la racine
EventNode * get_parent_node(EventNode *root,Point cle)
{
    EventNode * fd = root->right;
    EventNode * fg = root->left;
    if (point_prec(cle, root->key))
    {
        if(eq_key(fd->key, cle))
        {
            return root;
        }
        else
        {
            return get_parent_node(fd, cle);
        }
    }
    else
    {
        if (eq_key(fd->key, cle))
        {
            return root;
        }
        else
        {
            return get_parent_node(fg, cle);
        }
    }
}

//fonction a faire !!!! la fonction free()bug!!
//cette fonction permet de supprimer l'evenement prioritaire a partir d'un noeud classique et de le renvoyer
EventNode* get_next_event_aux(EventTree *a, EventNode *node)
{
    //sauvegarde du noeud
    EventNode *temp = new_event(node->key, node->type, node->s1, node->s2);
	// le cas ou le noeud ne possede aucun fils
	if(node->right==NULL && node->left==NULL)
	{

        if(node!=a->root)
        {
            EventNode * parent = get_parent_node(a->root, node->key);
            if (parent->right==node)
            {
                parent->right=NULL;
                free(node);
                return temp;
            }
            else
            {
                EventNode* temp2 = new_event(parent->key, parent->type, parent->s1, parent->s2);
                parent->key=node->key;
                parent->left=node->left;
                parent->right=node->right;
                parent->s1=node->s1;
                parent->s2=node->s2;
                parent->type=node->type;
                free(node);
                return temp2;
            }
        }
        else
        {
            a->root=NULL;
            free(node);
            return temp;
        }
	}
	// Le cas ou le noeud a seulment un fils gauche
	else if(node->right==NULL)
	{

        if (a->root==node) {
            a->root=node->left;
            free(node);
            return temp;
        }
        else
        {
            
            		//save des adresse des noeuds sur lequel nous allons faire un traitement
            		EventNode *pt_fils_gauche=node->left;
            		EventNode *pt_node = node;

            pt_node->key=pt_fils_gauche->key;
            pt_node->s1=pt_fils_gauche->s1;
            pt_node->s2=pt_fils_gauche->s2;
            pt_node->left=pt_fils_gauche->left;
            pt_node->right=pt_fils_gauche->right;
            pt_node->type=pt_fils_gauche->type;
            free(pt_fils_gauche);
            return temp;
        }
	}
	else
	{
		return get_next_event_aux(a,node->right);
	}
}
/*
 * trouve le prochain événement, le supprime de l'arbre et le renvoie
 * en somme, cette fonction supprime l'evenement le plus prioritaire,
 * eventuellement le fils le plus a droite, ou bien la racine 
 * puis renvoie l'event supprimé
 */
EventNode* get_next_event(EventTree *tree) {
	if(tree->size==0)
	{
		return NULL;
	}
	else
	{
		tree->size--;
		return get_next_event_aux(tree, tree->root);
	}
}




// cette fonction verifie la presence d'une clé à partir d'un pointeur sur noeud
//
int event_exists_aux(EventNode *node, Point key)
{
	//si le noeud pointe nulle part, alors key n'est pas presente
	if(node==NULL)
	{
		return 0;
	}
	else
	{
		//verifie si key est egale à la clé courante
		if(eq_key(key,node->key)==1)
		{
			return 1;
		}
		else
		{
			//si key est prioritaire sur le key courant alors nous cherchons dans le fils droit
			if(point_prec(key,node->key)==1)
			{
				return event_exists_aux(node->right,key);
			}
			//sinon dans le fils gauche
			else
			{
				return event_exists_aux(node->left,key);
			}
		}
	}
}

/*
 * renvoie 1 si le clef key existe dans l'ABR tree, O sinon
 */
int event_exists(EventTree *tree, Point key) {
	//si la taille est nul, key n'est pas dans l'arbre
	if (tree->size == 0)
	{
		return 0;
	}
	else
	{
		return event_exists_aux(tree->root, key);
	}
}
