#include <stdio.h>
#include <stdlib.h>
#include "geometry.h"

/*
 * affiche le point p
 */
void display_point(const Point p) {
  printf("(");
  display_rational(p.x);
  printf(",");
  display_rational(p.y);
  printf(")");
}

/*
 * affiche le segment s
 */
void display_segment(const Segment s) {
  printf("from: ");
  display_point(s.begin);
  printf(" to: ");
  display_point(s.end);
}

/*
 * renvoie 1 si le point key1 précède le point
 * key2, 0 sinon
 */
int point_prec(Point key1, Point key2) {
  if (eq(key1.x,key2.x))
  {
    return gt(key1.y,key2.y);
  }
  else
  {
    return lt(key1.x,key2.x);
  }
}
/*
 * renvoie 1 si s1 précède s2, 0 sinon
 */
int seg_prec(Segment s1, Segment s2, Rational x) {

  Rational e={(long)0.0000000000001,1};
  //dans ce cas, la droite de balayage ne touche aucun des deux segment ou bienne touche pas l'un des deux
  //nous ne pouvons donc comparer les droites
  if ((lt(x,min(s1.begin.x,s1.end.x)) || gt(x,(max(s1.begin.x,s1.end.x)))) || (lt(x,min(s2.begin.x,s2.end.x) )|| gt(x,max(s2.begin.x,s2.end.x))))
  {
   return 0;
  }
  //si il existe un point d'intersection entre la droite est le segment, alors nous calculons ses ordonnées
  else
  {
    Rational a1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
    Rational b1 = rsub(s1.begin.y,rmul(a1,s1.begin.x));
    Point * i = (Point *)malloc(sizeof(Point));
    i->y=radd(rmul(a1,x),b1);
    //i->x=rdiv(rsub(i->y,b1),a1);

    Rational a2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));
    Rational b2 = rsub(s2.begin.y,rmul(a2,s2.begin.x));
    Point * j = (Point *)malloc(sizeof(Point));
    j->y=radd(rmul(a2,x),b2);
    //j->x=rdiv(rsub(i->y,b2),a2);

    if(gt(i->y,j->y))
    {
      return 1;
    }
    else if (eq(i->y,j->y))
    {
      //dans le cas ou les ordonnées sont égaux alors nous recalculons les ordonnées en
      //déplacent legerement la droite de balayage de +0.000001, afin de pouvoir comparer les deux droite
      x=radd(x,e);
      Rational a1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
      Rational b1 = rsub(s1.begin.y,rmul(a1,s1.begin.x));
      Point * i = (Point *)malloc(sizeof(Point));
      i->y=radd(rmul(a1,x),b1);
      //i->x=rdiv(rsub(i->y,b1),a1);

      Rational a2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));
      Rational b2 = rsub(s2.begin.y,rmul(a2,s2.begin.x));
      Point * j = (Point *)malloc(sizeof(Point));
      j->y=radd(rmul(a2,x),b2);
      //j->x=rdiv(rsub(i->y,b2),a2);
      if(gt(i->y,j->y))
      {
        return 1;
      }
      else
      {
        return 0;
      }
    }
    else
    {
      return 0;
    }

  }
}

int orientation(Point p, Point q, Point r)
{
  Rational alpha_q_x = rsub(q.x,p.x);
  Rational alpha_q_y = rsub(q.y,p.y);
  Rational alpha_r_x = rsub(r.x,p.x);
  Rational alpha_r_y = rsub(r.y,p.y);

  Rational rep = rsub(rmul(alpha_q_x,alpha_r_y),rmul(alpha_r_x,alpha_q_y));

  return rep.num/rep.den;
}

int intersect(Segment s1, Segment s2)
{



  double alpha_r = orientation(s1.begin,s1.end,s2.begin);
  double alpha_s = orientation(s1.begin,s1.end,s2.end);
  double alpha_p = orientation(s2.begin,s2.end,s1.begin);
  double alpha_q = orientation(s2.begin,s2.end,s1.end);

  if ((alpha_r*alpha_s<0)&&(alpha_p*alpha_q<0))
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

Point* getIntersectionPoint(Segment s1, Segment s2) {
  //l'equation d'une des deux droite est de forme est de forme x=b
  if(eq(s1.begin.x,s1.end.x))
  {
    //Ceci est l'equation de la droite verticale
    Rational x1 = s1.begin.x;
    Rational a2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));
    Rational b2 = rsub(s2.begin.y,rmul(a2,s2.begin.x));
    Point * i = (Point *)malloc(sizeof(Point));
    i->y=radd(rmul(a2,x1),b2);
    i->x=rdiv(rsub(i->y,b2),a2);
    return i;
  }
  else if (eq(s2.begin.x,s2.end.x))
  {
    Rational x1 = s2.begin.x;
    Rational a2 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
    Rational b2 = rsub(s1.begin.y,rmul(a2,s1.begin.x));
    Point * i = (Point *)malloc(sizeof(Point));
    i->y=radd(rmul(a2,x1),b2);
    i->x=rdiv(rsub(i->y,b2),a2);
    return i;  
  }
  else
  {
    // a1 est le coefficient directeur de s1
    Rational a1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
    // b1 est l'ordonnée à l'origine de s1
    Rational b1 = rsub(s1.begin.y,rmul(a1,s1.begin.x));

    // a2 est le coefficient directeur de s1
    Rational a2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));
    // b2 est l'ordonnée à l'origine de s1
    Rational b2 = rsub(s2.begin.y,rmul(a2,s2.begin.x));

    // i est le point d'intersection
    Point * i = (Point *)malloc(sizeof(Point));
    i->x = rdiv(rsub(b2,b1),rsub(a1,a2));
    i->y = radd(rmul(a1,i->x),b1);

  return i;
  }
}


/*
 * renvoie 1 si s1 et s2 ont une intersection, 0 sinon
 */
